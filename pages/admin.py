from .models import Photo, Like, Comment
from django.contrib import admin
# Register your models here.

class CommentInline(admin.StackedInline):#admin.TabularInline):
    model = Comment


class LikeInline(admin.StackedInline):#admin.TabularInline):
    model = Like

class CommentAdmin(admin.ModelAdmin): # new
    inlines = [
    CommentInline,
    LikeInline
    ]


admin.site.register(Photo, CommentAdmin)
admin.site.register(Comment)
admin.site.register(Like)

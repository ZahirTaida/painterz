from .models import Photo, Comment, Like
from django import forms


class PhotoForm(forms.ModelForm):
    class Meta:
        model = Photo
        fields = ('name', 'photo',)


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('comment',)

class LikeForm(forms.ModelForm):
    class Meta:
        model = Like
        fields = ('like',)
from django.contrib.auth import get_user_model
from django.urls import reverse
from django.db import models
import uuid

# Create your models here.
class Photo(models.Model):
    id = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
        editable=False
        )
    name = models.CharField(max_length=100)
    date = models.DateField(auto_now_add=True)
    photo = models.ImageField(upload_to='img/')
    painted_photo = models.ImageField(upload_to='img/', blank=True, null=True)
    uploader = models.ForeignKey(
        get_user_model(),
        on_delete=models.CASCADE
        )

    class Meta:
        verbose_name_plural = "photos"

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('photo', args=[str(self.id)])


class Comment(models.Model):
    photo = models.ForeignKey(
        Photo, 
        on_delete=models.CASCADE,
        related_name='comments'
        )
    comment = models.CharField(max_length=140)
    user = models.ForeignKey(
        get_user_model(),
        on_delete=models.CASCADE,
    )
    created_at = models.DateTimeField(auto_now_add=True)


    class Meta:
        ordering = ['created_at']


    def __str__(self):
        return self.comment

class Like(models.Model):
    photo = models.ForeignKey(
        Photo, 
        on_delete=models.CASCADE,
        related_name='likes'
        )
    user = models.ForeignKey(
        get_user_model(),
        on_delete=models.CASCADE
    )
    like = models.BooleanField(default=False)

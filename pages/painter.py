from PIL import Image, ImageColor
from hashlib import sha256
import os

def get_position(image_src, colors, new_colors, bg_color, skip_draw, _range):
    for i in range(len(skip_draw)):
        if skip_draw[i] == 'true':
            skip_draw[i] = 'draw'

        else: 
            skip_draw[i] = 'skip'

    colors = [ImageColor.getcolor(color, "RGB") for color in colors]
    new_colors = [ImageColor.getcolor(new_color, "RGB") for new_color in new_colors]

    draw = Image.open(image_src) # Try to open image if it in the cwd.
    
    width, height = draw.size # Get image size

    pixelPose = [ImageColor.getcolor(bg_color, "RGB")] # Save pixel poses and colors

    # Check if the users want to ignore the color that he entred
    for i in range(len(colors)):
        s_d = skip_draw[i]
        # Get color range
        tend = _range[i]
        pixelPose.append(new_colors[i])
        color = colors[i]

        # Analyzing colors of pixels in image
        for x in range(width):
            for y in range(height):
                pixelRGB = draw.getpixel((x, y)) # Get pixel color in (x, y) position
                # add color if rgb of the pixel in the given range
                add = [True for i in range(3) if pixelRGB[i] in range(color[i] - tend, color[i] + tend)]
                if len(add) == 3 and s_d == 'draw': # add color
                    pixelPose.append((x, y))
                
                elif len(add) != 3 and s_d == 'skip': # skip color
                    pixelPose.append((x, y))


    if len(pixelPose) <= len(colors)-1: return False, False, False# If no pixels found with the given colors

    return pixelPose, width, height

def draw_image(pixelPose, width, height):
    data = f"{pixelPose}{width}{height}"
    img_hash = sha256(data.encode()).hexdigest()
    new_path = "./media/img/"+img_hash+'.png'
    board = Image.new('RGB', (width, height), tuple(pixelPose.pop(0))) # Open a board
    
    board.save(new_path) # save the board with the name
        
    
    # If user want to draw with paint

    color = 0
    for pos in pixelPose:

        try:
            if len(pos) == 3: # If it a color
                color = pos
            
            else: # if it a position
                try: board.putpixel((pos[0], pos[1]), tuple(color)) # draw the pixel with the current color
                except: pass
        
        except:
            color = [pos] * 3 # If it a color rgb with the same rate "0 convert (0, 0, 0)"

    board.save(new_path) # Save the image
    return new_path
from .views import (
    HomePageView, photo_detail, upload_photo, 
    painter_photo, profile, delete_view
    )
from django.urls import path


urlpatterns = [
    path('', HomePageView, name='home'),
    path('photos/<uuid:pk>/', photo_detail, name='photo'),
    path('photos/upload', upload_photo, name='upload'),
    path('photos/painter/<uuid:pk>/', painter_photo, name='painter'),
    path('profile/<str:username>/', profile, name='profile'),
    path('photos/delete/<uuid:pk>/', delete_view, name='delete'),
]
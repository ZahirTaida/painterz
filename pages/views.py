from django.shortcuts import redirect, render, get_object_or_404
from django.contrib.auth import get_user_model
from .painter import get_position, draw_image
from .forms import CommentForm, PhotoForm
from django.views.generic import ListView
from .models import Photo, Like


def HomePageView(request):

    template_name = 'home.html'
    return render(request, template_name, {'photos': Photo.objects.all()})

def photo_detail(request, pk):

    template_name = 'photo_detail.html'
    photo = get_object_or_404(Photo, pk=pk)
    likes = photo.likes.filter(like=True).count()
    comments = photo.comments.all()
    new_comment = None
    comment_form = CommentForm()
    if request.method == 'POST':
        if 'like' in request.POST:
            while True:
                try:
                    like = Like.objects.get(
                        user=request.user,
                        photo=photo
                        )
                    break
                except:
                    like = Like.objects.create(
                        user=request.user,
                        photo=photo
                        )
            like.like = False if like.like else True
            like.save()
            return redirect(photo_detail, photo.pk)


        else:
            comment_form = CommentForm(data=request.POST)
            if comment_form.is_valid():
                new_comment = comment_form.save(commit=False)
                new_comment.photo = photo
                new_comment.user = request.user
                new_comment.save()
                return redirect(photo_detail, photo.pk)
                
    
    if request.user.is_authenticated:
        is_liked = Like.objects.filter(user=request.user, photo=photo).values('like')
        is_liked = 'True' in str(is_liked)
    else: is_liked = False
    likes = photo.likes.filter(like=True).count()
    context = {
        'photo': photo, 'comments': comments, 
        'new_comment': new_comment, 
        'comment_form': comment_form,
        'likes': likes, 'is_liked': is_liked
            }

    return render(request, template_name, context)

def upload_photo(request):
    template_name = 'upload.html'

    if request.method == 'POST':
        photo_form = PhotoForm(request.POST, request.FILES)
        if photo_form.is_valid():
                new_photo = photo_form.save(commit=False)
                new_photo.uploader = request.user
                new_photo.save()
                return redirect(photo_detail, new_photo.pk)

    else:
        photo_form = PhotoForm()
                
    return render(request, template_name, {'photo_form': photo_form})

def painter_photo(request, pk):

    template_name = 'painter.html'
    photo=Photo.objects.get(pk=pk)
    message = ''
    
    if request.method == 'POST':
        try:
            img_src = './' + photo.photo.url
            colors = request.POST.get('color').split(' ')[:-1]
            new_colors = request.POST.get('new_color').split(' ')[:-1]
            bg_color = request.POST.get('bgColor')
            _ranges = [int(_range) for _range in request.POST.get('range').split(' ')[:-1]]
            cOption = request.POST.get('draw').split(' ')[:-1]
            pixelPose, width, height = get_position(
                img_src, colors, new_colors, 
                bg_color, cOption, _ranges
                )

            new_img = draw_image(pixelPose, width, height)
            photo.painted_photo = new_img.replace('/media/img', '/img')
            photo.save()
            message = ''
            return redirect(photo_detail, photo.pk)
        except KeyError:
            message = 'Fill all Fileds!'

    return render(request, template_name, {'photo': photo, 'message': message})

def profile(request, username):
    template_name = 'profile.html'
    uploader = get_user_model().objects.get(username=username)
    photos = Photo.objects.filter(uploader=uploader)
    return render(request, template_name, {'uploader': uploader, 'photos': photos})

def delete_view(request, pk):
    template_name = 'delete_photo.html'
    photo = Photo.objects.get(pk=pk)
    
    if request.method == 'POST':
        Photo.objects.get(pk=pk).delete()
        return redirect(HomePageView)

    return render(request, template_name, {'photo': photo})